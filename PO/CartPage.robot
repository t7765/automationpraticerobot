*** Settings ***
Library    SeleniumLibrary
Resource    ./BasePage.robot

*** Variables ***
${BTN.TRASH}                  css=a[title='Delete']  
${COUNT.PROCUCTS.CART}        css=div > a > span.ajax_cart_quantity
${QUANTITY.ITEM.CART}         css=.ajax_cart_no_product  
${MSG.CART.EMPTY}             css=p.alert-warning

*** Keywords ***
E clicar no icone de lixeira
    Click Element    ${BTN.TRASH}

Então o sistema deve remover o item que estava adicionado no carrinho
    Wait Until Element Is Visible    ${MSG.CART.EMPTY}
    ${QUANTITY}      Get Text    ${COUNT.PROCUCTS.CART}
    Should Not Be Equal          ${QUANTITY}    1

E o carrinho deve ficar com a contagem "empty"
    Wait Until Element Is Visible    ${QUANTITY.ITEM.CART}
    ${QUANTITY}     Get Text         ${QUANTITY.ITEM.CART}
    Should Be Equal                  ${QUANTITY}    (empty)

E deve exibir a mensagem "${MSG}"
    ${FEEDBACK}    Get Text    ${MSG.CART.EMPTY}
    Should Be Equal            ${FEEDBACK}    ${MSG}
    
    
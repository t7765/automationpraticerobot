*** Settings ***
Library    SeleniumLibrary
Resource    ./BasePage.robot

*** Variables ***
${URL}                               http://automationpractice.com/index.php
${MENU.WOMEN}                        css=li > a[title='Women']
${MENU.DRESSES}                      css=#block_top_menu > ul > li> a[title='Dresses']
${MENU.T-SHIRTS}                     css=#block_top_menu > ul > li> a[title='T-shirts']
${SUBMENU.W.TOP}                     css=li.sfHover > ul > li > a[title='Tops']
${SUBMENU.TOP.TSHIRT}                css=li.sfHover > ul > li > ul > li > a[title='T-shirts']  
${SUBMENU.TOP.BLOUSES}               css=li.sfHover > ul > li > ul > li > a[title='Blouses']
${SUBMENU.W.DRESSES}                 css=li.sfHover > ul > li > a[title='Dresses']
${SUBMENU.DRESSES.CASUALDRESSES}     css=li.sfHover > ul > li > ul > li > a[title='Casual Dresses']  
${SUBMENU.DRESSES.EVENINGDRESSES}    css=li.sfHover > ul > li > ul > li > a[title='Evening Dresses']  
${SUBMENU.DRESSES.SUMMERDRESSES}     css=li.sfHover > ul > li > ul > li > a[title='Summer Dresses'] 
${SEARCH}                            id=search_query_top 
${BTN.SEARCH}                        name=submit_search


*** Keywords ***
Dado que entrei na pagina inicial do site
    Go To       ${URL}

Quando carregar toda a pagina inicial
    Wait Until Element Is Visible    ${MENU.WOMEN}

Então o sistema deve apresentar na tela os menus em sequencia WOMEN, DRESSES E T-SHIRTS
    Element Should Be Visible    ${MENU.WOMEN}
    Element Should Be Visible    ${MENU.DRESSES}
    Element Should Be Visible    ${MENU.T-SHIRTS}

 Quando passar o mouse encima do menu WOMEN
     Mouse Over    ${MENU.WOMEN}

Então o sistema deve do lado esquerdo apresentar o item TOP
    Element Should Be Visible    ${SUBMENU.W.TOP}

 E deve apresentar abaixo dois sub menus T-shirts e Blouse
     Element Should Be Visible    ${SUBMENU.TOP.TSHIRT}
     Element Should Be Visible    ${SUBMENU.TOP.BLOUSES}

E do lado direito deve apresentar o item DRESSES
    Element Should Be Visible    ${SUBMENU.W.DRESSES}

E abaixo tres submenus Casual Dresses, Evening Dresses e Summer dresses
    Element Should Be Visible    ${SUBMENU.DRESSES.CASUALDRESSES}
    Element Should Be Visible    ${SUBMENU.DRESSES.EVENINGDRESSES}
    Element Should Be Visible    ${SUBMENU.DRESSES.SUMMERDRESSES}

E na barra de pesquisa pesquisei por "${PRODUTO}"
    Input Text    ${SEARCH}    ${PRODUTO}

Quando clicar no botão pesquisar
    Click Button    ${BTN.SEARCH}

E cliquei no botão de pesquisar
    Quando clicar no botão pesquisar

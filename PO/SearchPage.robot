*** Settings ***
Library    SeleniumLibrary
Library    String
Resource    ./BasePage.robot
Resource    ./HomePage.robot

*** Variables ***
${RESULTS.FOUND.COUNT}           css=#center_column > h1 > span.heading-counter
${NO.RESULTS.FOUND}              0 results have been found.
@{PRODUCTS}                      css=div.right-block > h5 > a
${PRICE.BEFORE}                  1.0
${FIRST.PRODUCT.IMG}             xpath=//*[@id="center_column"]/ul/li[1]//*[@src]
${BTN.ADD.CART}                  xpath=//*[@id="center_column"]/ul/li[1]/div/div[2]/div[2]/a[1]/span
${FEEDBACK.ADD.CART}             css=div.layer_cart_product.col-xs-12.col-md-6 > h2
${CLOSE.CART}                    css=span[title='Close window']
${COUNT.ITEM.CART}               css=span.ajax_cart_quantity.unvisible
${BTN.CART}                      css=a[title='View my shopping cart']
${LIST.PRODUCTS}                 xpath=//*[@id="center_column"]/ul/li/div/div[2]/h5/a

*** Keywords ***
Então o sistema deve carregar os produtos encontrados
    Wait Until Element Is Visible    ${RESULTS.FOUND.COUNT}

E o resultado retornando não pode ser 0
    Should Not Be Equal    ${RESULTS.FOUND.COUNT}    ${NO.RESULTS.FOUND}

E os produtos devem possuir nome 
    ${LIST} =       Create List          ${LIST.PRODUCTS}
    ${SIZELIST}=    Get Element Count    ${LIST}
    FOR  ${i}  IN RANGE     1     ${SIZELIST}
        ${name}    Get Text    //*[@id="center_column"]/ul/li[${i}]/div/div[2]/h5/a
        IF    "${name}" == ""
            Fail    
        END
    END

E os produtos devem conter imagens
    ${LIST} =       Create List          ${LIST.PRODUCTS}
    ${SIZELIST}=    Get Element Count    ${LIST}
     FOR  ${i}  IN RANGE     1     ${SIZELIST}
        ${img}    Get Value    //*[@id="center_column"]/ul/li[${i}]//*[@src]
        IF    "${img}" == ""
            Fail    
        END
    END
    Capture Page Screenshot

Quado carregar os resultados encontrados
    Então o sistema deve carregar os produtos encontrados

E clicar em Sorty by escolhendo a opção Price:Lowest first
    Click Element    //*[@id="selectProductSort"]/option[contains(.,'Price: Lowest first')]

Então o sistema deve reordenar os produtos de forma que seja mostrado do menor valor para o maior valor
    ${LIST} =       Create List          ${LIST.PRODUCTS}
    ${SIZELIST}=    Get Element Count    ${LIST}
     FOR  ${i}  IN RANGE    1    ${SIZELIST}
        ${price}    Get Text    //*[@id="center_column"]/ul/li[${i}]/div/div[2]/div[1]/span
        ${new.price}    Remove String    ${price}    $
        IF    ${new.price} < ${PRICE.BEFORE}
            Log Many    ${new.price}    ${PRICE.BEFORE}
            Capture Page Screenshot
            Fail    
        END
        ${PRICE.BEFORE}       Get Variable Value    ${new.price}
     END

Quando passar o mouse encima do resultado encontrado
    Mouse Over        ${FIRST.PRODUCT.IMG}

E clicar em Add to cart
    Wait Until Element Is Visible    ${BTN.ADD.CART}
    Click Element                    ${BTN.ADD.CART}

Então o sistema deve abrir um modal exibindo a mensagem "${MSG}"
    Wait Until Element Is Visible    ${FEEDBACK.ADD.CART}
    ${MSG.FEEDBACK}    Get Text      ${FEEDBACK.ADD.CART}
    Should Be Equal                  ${MSG.FEEDBACK}    ${MSG}

E ao fechar o modal, o sistema deve contabilizar um item no carrinho
    Click Element                    ${CLOSE.CART}
    ${QUANTITY}    Get Text          ${COUNT.ITEM.CART}
    Should Be Equal                  ${QUANTITY}    1

E que ja tenha adicionado um item no carrinho em uma visita anteriormente
    E na barra de pesquisa pesquisei por "Blouse"
    E cliquei no botão de pesquisar
    Quando passar o mouse encima do resultado encontrado
    E clicar em Add to cart
    Wait Until Element Is Visible    ${CLOSE.CART}    timeout=10
    E ao fechar o modal, o sistema deve contabilizar um item no carrinho

Quando clicar no carrinho
    Click Element    ${BTN.CART}
    
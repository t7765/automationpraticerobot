*** Settings ***
Resource    ../PO/BasePage.robot
Resource    ../PO/HomePage.robot
Resource    ../PO/SearchPage.robot
Resource    ../PO/CartPage.robot

Test Setup       Abrir navegador
Test Teardown    Close Browser

*** Test Cases ***

CT 1: Verificar menus Women, dresses e Tshirt
    Dado que entrei na pagina inicial do site
    Quando carregar toda a pagina inicial
    Então o sistema deve apresentar na tela os menus em sequencia WOMEN, DRESSES E T-SHIRTS

CT 2: Verificar submenu Women
    Dado que entrei na pagina inicial do site
    Quando passar o mouse encima do menu WOMEN
    Então o sistema deve do lado esquerdo apresentar o item TOP
    E deve apresentar abaixo dois sub menus T-shirts e Blouse
    E do lado direito deve apresentar o item DRESSES
    E abaixo tres submenus Casual Dresses, Evening Dresses e Summer dresses


CT 3: Pesquisar por um vestido
    Dado que entrei na pagina inicial do site
    E na barra de pesquisa pesquisei por "Printed Dress"
    Quando clicar no botão pesquisar
    Então o sistema deve carregar os produtos encontrados
    E o resultado retornando não pode ser 0
    E os produtos devem possuir nome
    E os produtos devem conter imagens

CT 4: Pesquisar por um vestido e filtrar pelo menor preço
    Dado que entrei na pagina inicial do site
    E na barra de pesquisa pesquisei por "Printed Dress"
    E cliquei no botão de pesquisar
    Quado carregar os resultados encontrados
    E clicar em Sorty by escolhendo a opção Price:Lowest first
    Então o sistema deve reordenar os produtos de forma que seja mostrado do menor valor para o maior valor


CT 5: Adicionar um produto no carrinho
    Dado que entrei na pagina inicial do site
    E na barra de pesquisa pesquisei por "Blouse"
    E cliquei no botão de pesquisar
    Quando passar o mouse encima do resultado encontrado
    E clicar em Add to cart
    Então o sistema deve abrir um modal exibindo a mensagem "Product successfully added to your shopping cart"
    E ao fechar o modal, o sistema deve contabilizar um item no carrinho

CT 6: Remover um produto do carrinho
    Dado que entrei na pagina inicial do site
    E que ja tenha adicionado um item no carrinho em uma visita anteriormente
    Quando clicar no carrinho
    E clicar no icone de lixeira
    Então o sistema deve remover o item que estava adicionado no carrinho
    E o carrinho deve ficar com a contagem "empty"
    E deve exibir a mensagem "Your shopping cart is empty."
